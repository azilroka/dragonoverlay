## Interface: 60000
## Title: Dragon Overlay
## Version: 1.93
## Author: Azilroka / Credits: Codeblake, Kkthnxbye, Narley and Durandil
## Notes: Provides an overlay on UnitFrames for Boss/Elite/Rare/RareElite
## SavedVariables: DragonOverlayOptions
## OptionalDeps: Tukui, ElvUI, AsphyxiaUI, DuffedUI
## X-Tukui-ProjectID: 136
## X-Tukui-ProjectFolders: DragonOverlay

DragonOverlay.lua
